<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * FilieresInstitutions Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Filieres
 * @property \Cake\ORM\Association\BelongsTo $Institutions
 *
 * @method \App\Model\Entity\FilieresInstitution get($primaryKey, $options = [])
 * @method \App\Model\Entity\FilieresInstitution newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\FilieresInstitution[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\FilieresInstitution|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\FilieresInstitution patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\FilieresInstitution[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\FilieresInstitution findOrCreate($search, callable $callback = null)
 */
class FilieresInstitutionsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('filieres_institutions');

        $this->belongsTo('Filieres', [
            'foreignKey' => 'filiere_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Institutions', [
            'foreignKey' => 'institution_id',
            'joinType' => 'INNER'
        ]);
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['filiere_id'], 'Filieres'));
        $rules->add($rules->existsIn(['institution_id'], 'Institutions'));

        return $rules;
    }
}
