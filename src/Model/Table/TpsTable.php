<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Tps Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Modules
 * @property \Cake\ORM\Association\BelongsToMany $Groups
 *
 * @method \App\Model\Entity\Tp get($primaryKey, $options = [])
 * @method \App\Model\Entity\Tp newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Tp[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Tp|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Tp patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Tp[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Tp findOrCreate($search, callable $callback = null)
 */
class TpsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('tps');
        $this->displayField('title');
        $this->primaryKey('id');

        $this->belongsTo('Modules', [
            'foreignKey' => 'module_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsToMany('Groups', [
            'foreignKey' => 'tp_id',
            'targetForeignKey' => 'group_id',
            'joinTable' => 'tps_groups'
        ]);

        $this->belongsToMany('Users', [
            'foreignKey' => 'tp_id',
            'targetForeignKey' => 'user_id',
            'joinTable' => 'tps_users'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->uuid('id')
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('title', 'create')
            ->notEmpty('title');

        $validator
            ->requirePresence('description', 'create')
            ->notEmpty('description');

        $validator
            ->dateTime('created_at')
            //->requirePresence('created_at', 'create')
            ->allowEmpty('created_at');

        $validator
            ->dateTime('deadline')
            ->requirePresence('deadline', 'create')
            ->notEmpty('deadline');

        $validator
            ->integer('enonce')
            ->allowEmpty('enonce', 'create');
        
        $validator
            ->allowEmpty('filename');
        
        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['module_id'], 'Modules'));

        return $rules;
    }
}
