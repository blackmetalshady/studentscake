<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Filieres Model
 *
 * @property \Cake\ORM\Association\HasMany $Groups
 * @property \Cake\ORM\Association\HasMany $Modules
 * @property \Cake\ORM\Association\BelongsToMany $Institutions
 *
 * @method \App\Model\Entity\Filiere get($primaryKey, $options = [])
 * @method \App\Model\Entity\Filiere newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Filiere[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Filiere|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Filiere patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Filiere[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Filiere findOrCreate($search, callable $callback = null)
 */
class FilieresTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('filieres');
        $this->displayField('name');
        $this->primaryKey('id');

        $this->hasMany('Groups', [
            'foreignKey' => 'filiere_id'
        ]);
        $this->hasMany('Modules', [
            'foreignKey' => 'filiere_id'
        ]);
        $this->belongsToMany('Institutions', [
            'foreignKey' => 'filiere_id',
            'targetForeignKey' => 'institution_id',
            'joinTable' => 'filieres_institutions'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->uuid('id')
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('name', 'create')
            ->notEmpty('name');

        $validator
            ->requirePresence('shortname', 'create')
            ->notEmpty('shortname');

        $validator
            ->integer('years_of_study')
            ->requirePresence('years_of_study', 'create')
            ->notEmpty('years_of_study');

        return $validator;
    }
}
