<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * TpsGroup Entity
 *
 * @property string $group_id
 * @property string $tp_id
 * @property \Cake\I18n\Time $start_time
 *
 * @property \App\Model\Entity\Group $group
 * @property \App\Model\Entity\Tp $tp
 */
class TpsGroup extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'group_id' => false,
        'tp_id' => false
    ];
}
