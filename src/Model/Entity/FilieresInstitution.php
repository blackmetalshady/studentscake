<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * FilieresInstitution Entity
 *
 * @property string $filiere_id
 * @property string $institution_id
 *
 * @property \App\Model\Entity\Filiere $filiere
 * @property \App\Model\Entity\Institution $institution
 */
class FilieresInstitution extends Entity
{

}
