<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * FilieresModule Entity
 *
 * @property string $modules_id
 * @property string $filiere_id
 *
 * @property \App\Model\Entity\Module $module
 * @property \App\Model\Entity\Filiere $filiere
 */
class FilieresModule extends Entity
{

}
