<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * UsersGroup Entity
 *
 * @property string $user_id
 * @property string $group_id
 *
 * @property \App\Model\Entity\User $user
 * @property \App\Model\Entity\Group $group
 */
class UsersGroup extends Entity
{

}
