<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Event\Event;
use Cake\Network\Exception\UnauthorizedException;
use Cake\Utility\Security;
use Firebase\JWT\JWT;

/**
 * Users Controller
 *
 * @property \App\Model\Table\UsersTable $Users
 */
class UsersController extends AppController
{
    public function beforeFilter(Event $event)
    {
        parent::beforeFilter($event);
        $this->Auth->allow(['logout','login','add','isSessionValid']);
    }

    public function initialize()
    {
        parent::initialize();
        $this->Auth->allow(['add', 'token']);
    }

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $users = $this->paginate($this->Users);

        $this->set(compact('users'));
        $this->set('_serialize', ['users']);
    }

    /**
     * View method
     *
     * @param string|null $id User id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $user = $this->Users->get($id, [
            'contain' => []
        ]);

        $this->set('user', $user);
        $this->set('_serialize', ['user']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
    	$this->Crud->on('afterSave', function(Event $event) {
	        if ($event->subject->created) {
	            $this->set('data', [
	                'id' => $event->subject->entity->id,
	                'token' => JWT::encode(
	                    [
	                        'sub' => $event->subject->entity->id,
	                        'exp' =>  time() + 604800
	                    ],
	                Security::salt())
            	]);
            	$this->Crud->action()->config('serialize.data', 'data');
        	}
   		});
    	return $this->Crud->execute();
        /*$user = $this->Users->newEntity();
        if ($this->request->is('post')) {
            $user = $this->Users->patchEntity($user, $this->request->data);
            if ($this->Users->save($user)) {
                $this->Flash->success(__('The user has been saved.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The user could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('user'));
        $this->set('_serialize', ['user']);*/
    }

    /**
     * Edit method
     *
     * @param string|null $id User id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $user = $this->Users->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $user = $this->Users->patchEntity($user, $this->request->data);
            if ($this->Users->save($user)) {
                $this->Flash->success(__('The user has been saved.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The user could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('user'));
        $this->set('_serialize', ['user']);
    }

    /**
     * Delete method
     *
     * @param string|null $id User id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $user = $this->Users->get($id);
        if ($this->Users->delete($user)) {
            $this->Flash->success(__('The user has been deleted.'));
        } else {
            $this->Flash->error(__('The user could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }

    public function login()
    {
        //echo "dakhala hona";
        //die();
        $data = [];
        if (!$this->Auth->user()) {
            if ($this->request->is('post')) {
                $user = $this->Auth->identify();
                if ($user) {
                    if ($user['enabled'] == '0') {
                        $data = ['status'=>false,'message'=>'Ce Compte est desactiver, veuillez contacter votre formateur'];
                    }else{
                        $this->Auth->setUser($user);
                        $data = ['status'=>true,'message'=>'ok','data'=>$user];
                    }
                }else{
                    $data = ['status'=>false,'message'=>'E-mail ou Mot de passe Incorrect'];
                }
            }else{
            	$data = ['status'=>false,'message'=>'Requet Get innaccepté'];
            }
        }else{
            $data = ['status'=>false,'message'=>'E-mail ou Mot de passe Incorrect'];
        }
        //print_r($data); die();
        $this->set(compact('data'));
        $this->set('_serialize', ['data']);
        //echo json_encode($data);
        //die();
    }

    public function isSessionValid(){
    	if ($this->Auth->user()) {
    		//$data = ['status'=>true,'message'=>'User is Logged'];
    	}else{
    		//$data = ['status'=>false,'message'=>'User is NOT Logged'];
    	}
    	print_r($this->request->session()); die();
    	$this->set(compact('data'));
        $this->set('_serialize', ['data']);
    }
    public function logout()
    {
        if($this->Auth->logout()){
            $data = ['status'=>true,'message'=>'ok'];
        }else{
        	$data = ['status'=>false,'message'=>'Erreur'];
        }

        $this->set(compact('data'));
        $this->set('_serialize', ['data']);
    }
}
