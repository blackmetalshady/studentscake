<?php
namespace App\Controller\Api;

use App\Controller\Api\AppController;
use Cake\Filesystem\File;

/**
 * Materials Controller
 *
 * @property \App\Model\Table\MaterialsTable $Materials
 */
class MaterialsController extends AppController
{

    public function initialize()
    {
        parent::initialize();
        if($this->request->is('options')) {
            $this->response->statusCode(204);
            $this->response->send();
            die();
        }
        $this->Auth->allow(['getMaterials']);
    }
    /**
     * View method
     *
     * @param string|null $id Material id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $material = $this->Materials->get($id, [
            'contain' => ['Modules']
        ]);

        $this->set('material', $material);
        $this->set('_serialize', ['material']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
        public function add()
    {
        $status = false;
        if ($this->request->is('post')) {

            
            //print_r($this->request->data); die();

            if(!empty($this->request->data['file']['tmp_name'])){
                $new_material = $this->Materials->newEntity();
                $new_material = $this->Materials->patchEntity($new_material, $this->request->data);

                if ($material = $this->Materials->save($new_material)) {
                    $filename   = $this->request->data['file']['name'];
                    $extension  = strtolower(pathinfo($filename, PATHINFO_EXTENSION));
                    $newname    = str_replace(' ', '_', $material->title).'.'.$extension;
                    $finalfile = WWW_ROOT.'files/materials/'.$newname;
                    if (move_uploaded_file($this->request->data['file']['tmp_name'], $finalfile)) {
                        $upMaterial = $this->Materials->get($material->id);
                        $upMaterial = $this->Materials->patchEntity($upMaterial, ['filename'=>$newname]);
                        if ($this->Materials->save($upMaterial)) {
                            $status = true;
                        }else{
                            print_r($material); die();
                        }
                    }else{
                        echo "move fail"; die();
                        $this->Materials->delete($material->id);
                    }
                }else{
                    print_r($new_material); die();
                    echo "sauvegarde fail"; die();
                }
            }else{
                //print_r($this->request->data); die();
                $material = $this->Materials->newEntity();
                $material = $this->Materials->patchEntity($material, $this->request->data);
                if ($this->Materials->save($material)) {
                    $status = true;
                }else{
                    echo "file Empty";die();
                }
            }
        }else{
            echo "POST FAIL"; die();
        }
        $this->set(compact('status'));
        $this->set('_serialize', ['status']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Material id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $material = $this->Materials->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $material = $this->Materials->patchEntity($material, $this->request->data);
            if ($this->Materials->save($material)) {
                $this->Flash->success(__('The material has been saved.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The material could not be saved. Please, try again.'));
            }
        }
        $modules = $this->Materials->Modules->find('list', ['limit' => 200]);
        $this->set(compact('material', 'modules'));
        $this->set('_serialize', ['material']);
    }

    public function getMaterials($group_id){
        $this->loadModel('Groups');
        $group = $this->Groups->get($group_id, [
                'contain' => ['Modules'=>['Materials']]
                ]);
        $modules = $group->modules;
        $this->set(compact('modules'));
        $this->set('_serialize', ['modules']);
    }

    public function getMaterialsByFiliere(){
        if ($this->request->is(['post'])) {
            //$data = ['test'=>'hello'];
            /*$materials = $this->Materials->find()->matching("Modules", 
                         function($q)  {
                            return $q->where(["Modules.filiere_id"=>$this->request->data['filiere_id'],
                                               "Modules.year"=>$this->request->data['year'] 
                                             ]);
                         }
                      );*/
            $this->loadModel('Modules');
            $modules = $this->Modules->find()
                                     ->contain(['Materials'=>[
                                                  'sort'=>['created_at'=>'DESC']
                                                ]
                                              ])
                                     ->where([
                                        "Modules.filiere_id"=>$this->request->data['filiere_id'],
                                        "Modules.year"=>$this->request->data['year']
                                      ]);
            //print_r($modules); die();
            $this->set(compact('modules'));
            $this->set('_serialize', ['modules']);
        }
    }

    public function delete(){
        if ($this->request->is(['post'])) {
            $status = false;
            $material = $this->Materials->get($this->request->data['id']);
            $filename = $material->filename;
            if ($this->Materials->delete($material)) {
                $file = new File(WWW_ROOT.'files/materials/'.$filename, false, 0777);
                if($file->delete()) {
                   $status = true;
                }
            }
        }
        $this->set(compact('status'));
        $this->set('_serialize', ['status']);
    }
}
