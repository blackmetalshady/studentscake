<?php
namespace App\Controller\Api;

use App\Controller\Api\AppController;
use Cake\Event\Event;
use Cake\I18n\Time;

/**
 * Tps Controller
 *
 * @property \App\Model\Table\TpsTable $Tps
 */
class TpsController extends AppController
{

    public function initialize()
    {
        parent::initialize();
        if($this->request->is('options')) {
            $this->response->statusCode(204);
            $this->response->send();
            die();
        }
        $this->Auth->allow(['add']);
    }

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        /*$this->paginate = [
            'contain' => ['Filieres']
        ];
        $tps = $this->paginate($this->Tps);*/
        $tps = $this->Tps->find();
        $this->set(compact('tps'));
        $this->set('_serialize', ['tps']);
    }

    public function tpsByFiliere($filiere_id, $year){
        $allTps = ['A','B','C','D','E','F','G','H','I','J','K'];
        $tps = $this->Tps->find()
                               ->select(['letter'])
                               ->where(['filiere_id'=>$filiere_id, 'year'=>$year])
                               ->toArray();

        foreach ($tps as $key => $value) {
            if (false !== $k = array_search($value->letter, $allTps)) {
                unset($allTps[$k]);
            }
        }
        $allTps = array_values($allTps);
        $this->set(compact('allTps'));
        $this->set('_serialize', ['allTps']);
    }

    /**
     * View method
     *
     * @param string|null $id Group id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $tp = $this->Tps->get($id, [
            'contain' => ['Filieres', 'Users']
        ]);

        $this->set('tp', $tp);
        $this->set('_serialize', ['tp']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $status = false;
        $allgroups = [];
        if ($this->request->is('post')) {

            if (!empty($this->request->data['groups'])) {
                $groups = json_decode($this->request->data['groups']);

                foreach ($groups as $grp) {
                    array_push($allgroups, ['id'=>$grp->id, 'start_time'=>date('Y-m-d H:m')]);
                    //$allgroups[] = ['id'=>$grp->id, 'start_time'=>date()];
                }
                /*for($i=0;$i<sizeof($groups); $i++) {
                    $groups[$i]['start_time'] = date();
                }*/
                $this->request->data['groups'] = $allgroups;
            }
            
            //print_r($this->request->data); die();

            if(!empty($this->request->data['file']['tmp_name'])){
                $ntp = $this->Tps->newEntity();
                $this->request->data['created_at'] = Time::now();
                $ntp = $this->Tps->patchEntity($ntp, $this->request->data);

                if ($tp = $this->Tps->save($ntp)) {
                    $filename   = $this->request->data['file']['name'];
                    $extension  = strtolower(pathinfo($filename, PATHINFO_EXTENSION));
                    $newname    = $tp->id.'.'.$extension;
                    $finalfile = WWW_ROOT.'files/enonce/'.$newname;
                    if (move_uploaded_file($this->request->data['file']['tmp_name'], $finalfile)) {
                        $upTp = $this->Tps->get($tp->id);
                        $upTp = $this->Tps->patchEntity($upTp, ['enonce'=>'1', 'filename'=>$newname]);
                        if ($this->Tps->save($upTp)) {
                            $status = true;
                        }else{
                            print_r($tp); die();
                        }
                    }else{
                        echo "move fail"; die();
                        $this->Tps->delete($tp->id);
                    }
                }else{
                    print_r($ntp); die();
                    echo "sauvegarde fail"; die();
                }
            }else{
                //print_r($this->request->data); die();
                $tp = $this->Tps->newEntity();
                $tp = $this->Tps->patchEntity($tp, $this->request->data);
                if ($this->Tps->save($tp)) {
                    $status = true;
                }
            }
        }
        $this->set(compact('status'));
        $this->set('_serialize', ['status']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Group id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit()
    {
        //print_r($this->request->data); die();
        $id = $this->request->data['id'];
        //echo $id; die();
        $tp = $this->Tps->get($id, [
            'contain' => []
        ]);

        $status = false;
        if ($this->request->is(['patch', 'post', 'put'])) {
            $this->request->data['deadline'] .= ' '.$this->request->data['deadline_time'];
            $tp = $this->Tps->patchEntity($tp, $this->request->data);
            if ($this->Tps->save($tp)) {
                $status = true;
            } 
        }
        $this->set(compact('status'));
        $this->set('_serialize', ['status']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Group id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $status = false;
        //$this->request->allowMethod(['post', 'delete']);
        $tp = $this->Tps->get($id);
        if ($this->Tps->delete($tp)) {
            $status = true;
        } 
        $this->set(compact('status'));
        $this->set('_serialize', ['status']);
    }

    public function getUsersTps($group_id = null, $tp_id = null){
        $now = Time::now();
        $tps = [];
        $expiredtps = [];

        $this->loadModel('Users');
        $users = $this->Users->find()
                            ->contain(['Tps'=>function ($q) use($tp_id){
                                       return $q
                                                ->where(['Tps.id' => $tp_id]);
                                    }
                                ])
                            ->where(['group_id'=>$group_id])
                            ->order(['lastname'=>'ASC']);
        
        $this->set(compact('users'));
        $this->set('_serialize', ['users']);
    }

    public function upload(){
        $status = false;
        if ($this->request->is('post')) {
            //$upload_dir = WWW_ROOT.'img/sliders/';
            $filename   = basename($this->request->data['file']['name']);
            $extension  = strtolower(pathinfo($filename, PATHINFO_EXTENSION));
            $newname    = $this->request->data['user_id'].'_'.$this->request->data['tp_id'].'.'.$extension;
            $finalfile = WWW_ROOT.'files/tps/'.$newname;

            if(move_uploaded_file($this->request->data['file']['tmp_name'], $finalfile)){
                $this->loadModel('TpsUsers');
                $tp = $this->TpsUsers->newEntity();
                $tp = $this->TpsUsers->patchEntity($tp, ['user_id'=>$this->request->data['user_id'],
                                                         'tp_id'=>$this->request->data['tp_id'],
                                                         'filename'=>$newname,
                                                         'uploaded'=>'1']);
                if($this->TpsUsers->save($tp)){
                    $status = true;
                }
            }
        }
        $this->set(compact('status'));
        $this->set('_serialize', ['status']);
    }
}
