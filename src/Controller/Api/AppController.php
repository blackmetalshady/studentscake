<?php
namespace App\Controller\Api;

use Cake\Controller\Controller;
use Cake\Event\Event;
use Cake\Routing\Router;
use Cake\Mailer\Email;

class AppController extends Controller
{

    use \Crud\Controller\ControllerTrait;

    public function initialize()
    {
        parent::initialize();

        $this->loadComponent('RequestHandler');
        $this->loadComponent('Flash');
        $this->loadComponent('Crud.Crud', [
            'actions' => [
                'Crud.Index',
                'Crud.View',
                'Crud.Add',
                'Crud.Edit',
                'Crud.Delete'
            ],
            'listeners' => [
                'Crud.Api',
                'Crud.ApiPagination',
                'Crud.ApiQueryLog'
            ]
        ]);
        $this->loadComponent('Auth', [
            'storage' => 'Memory',
            'authenticate' => [
                'Form' => [
                    //'scope' => ['Users.enabled' => 1],
                    'fields' => ['username'=>'email']
                ],
                'ADmad/JwtAuth.Jwt' => [
                    'parameter' => 'token',
                    'userModel' => 'Users',
                    //'scope' => ['Users.enabled' => 1],
                    'fields' => [
                        'username' => 'id'
                    ],
                    'queryDatasource' => true
                ]
            ],
            'unauthorizedRedirect' => false,
            'checkAuthIn' => 'Controller.initialize'
        ]);
    }


    public function beforeFilter(event $event) {

        if($this->request->is('options')) {
            return $this->response;
        }
    }

    public function sendverifyemail($user, $token){

            $url = Router::url([
                '_full'=>true,
                '_name' => 'verify' //configured in Routes
            ]);
            $url .= $user->id.'/'.$user->registrationtoken;
            $message = '<center><img src="http://'.$this->request->env('HTTP_HOST').'/img/logo.jpg" style="width: 220px"></center><br><br><span style="font-size: 18px; margin-bottom: 20px;">'.__('To complet your registration please follow or copy/past the link bellow to your browser').' : </span><br><br><a href="'.$url.'" style="font-size: 18px;">'.$url.'</a><br><br><span style="font-size: 18px; margin-top: 20px;">if you\'re not the source of this request, please make sur to <a href="http://'.$this->request->env('HTTP_HOST').'/login">change your password</a></span>';
        //echo $message; die();
        $email = new Email('default');
        $email->from(['info@ramcarloc.com' => __('Travail Pratique')])
            ->emailFormat('html')
            ->to($user->email)
            ->subject('Travail Pratique: '.__('Veuillez confirmer votre inscription'));
        if ($email->send($message)) {
            return true;
        }else{
            return false;
        }
    }
    /*public function beforeRender(Event $event)
    {
        if (!array_key_exists('_serialize', $this->viewVars) &&
            in_array($this->response->type(), ['application/json', 'application/xml'])
        ) {
            $this->set('_serialize', true);
        }
    }*/
}