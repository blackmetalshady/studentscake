<?php
namespace App\Controller\Api;

use App\Controller\Api\AppController;
use Cake\Event\Event;

/**
 * Modules Controller
 *
 * @property \App\Model\Table\ModulesTable $Modules
 */
class ModulesController extends AppController
{

    public function initialize()
    {
        parent::initialize();
        if($this->request->is('options')) {
            $this->response->statusCode(204);
            $this->response->send();
            die();
        }
        $this->Auth->allow(['index','getModulesByGroup']);
    }

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $modules = $this->Modules->find()
                                   ->contain(['Groups'=>[
                                            'Tps'=>[
                                                'sort'=>['deadline'=>'DESC']
                                            ],
                                            'Modules'=>[
                                                'sort'=>['shortname'=>'DESC']
                                            ],
                                            'sort'=>['letter'=>'ASC']
                                        ]
                                    ]);

        $this->set(compact('modules'));
        $this->set('_serialize', ['modules']);
    }


    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $module = $this->Modules->newEntity();
        $allGroups = [];
        $status = false;
        if ($this->request->is('post')) {
            if (!empty($this->request->data['groups'])) {
                $groups = json_decode($this->request->data['groups']);
                //print_r($groups); die();
                foreach ($groups as $grp) {
                    array_push($allGroups, ['id'=>$grp->id]);
                }
                $this->request->data['groups'] = $allGroups;
            }
            $module = $this->Modules->patchEntity($module, $this->request->data);
            if ($this->Modules->save($module)) {
                $status = true;
            } 
        }
        $this->set(compact('status'));
        $this->set('_serialize', ['status']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Module id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $module = $this->Modules->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $module = $this->Modules->patchEntity($module, $this->request->data);
            if ($this->Modules->save($module)) {
                $this->Flash->success(__('The module has been saved.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The module could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('module'));
        $this->set('_serialize', ['module']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Module id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $module = $this->Modules->get($id);
        if ($this->Modules->delete($module)) {
            $this->Flash->success(__('The module has been deleted.'));
        } else {
            $this->Flash->error(__('The module could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }

    public function deleteModuleForGroup(){
      if ($this->request->is(['post'])) {
        //print_r($this->request->data); die();
        $status = false;
        $this->loadModel("ModulesGroups");
        /*$record = $this->ModulesGroups->find()
                                      ->where(['module_id'=>$this->request->data['module_id'], 
                                               'group_id'=>$this->request->data['group_id']
                                        ])
                                      ->first();*/
        if ($this->ModulesGroups->deleteAll($this->request->data)) {
          $status = true;
        }

        $this->set(compact('status'));
        $this->set('_serialize', ['status']);
      }
    }

    public function getModulesByGroup($group_id = null){
        $this->loadModel('Groups');
        $group = $this->Groups->get($group_id, [
                'contain' => ['Modules','Tps'=>[
                                            'Users',
                                            'sort'=>['deadline'=>'DESC']
                                        ]
                ]
            ]);

        $this->set(compact('group'));
        $this->set('_serialize', ['group']);
    }

    public function getAll(){
        $modules = $this->Modules->find();

        $this->set(compact('modules'));
        $this->set('_serialize', ['modules']);
    }

    public function getModulesByFiliereYear(){
        if ($this->request->is(['post'])) {
            $modules = $this->Modules->find()->where([
                'filiere_id'=>$this->request->data['filiere_id'],
                'year'=>$this->request->data['year']
            ])->order(['name'=>'ASC']);

            $this->set(compact('modules'));
            $this->set('_serialize', ['modules']);
        }
    }
}
