<?php
namespace App\Controller\Api;

use App\Controller\Api\AppController;
use Cake\Mailer\Email;

/**
 * Messages Controller
 *
 * @property \App\Model\Table\MessagesTable $Messages
 */
class MessagesController extends AppController
{

    public function initialize()
    {
        parent::initialize();
        if($this->request->is('options')) {
            $this->response->statusCode(204);
            $this->response->send();
            die();
        }
        //$this->Auth->allow(['add', 'token', 'exists','verify']);
    }

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Teachers', 'Students', 'Modules']
        ];
        $messages = $this->paginate($this->Messages);

        $this->set(compact('messages'));
        $this->set('_serialize', ['messages']);
    }


    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        if ($this->request->is('post')) {
            $status = false;
            if ($usr = $this->Auth->user()) {
                $this->request->data['from_user'] = $usr['id'];
                $this->request->data['to_user'] = $this->request->data['teacher'];
                $message = $this->Messages->newEntity();
                $message = $this->Messages->patchEntity($message, $this->request->data);
                if ($msg = $this->Messages->save($message)) {
                    $this->loadModel("Users");
                    $teacher = $this->Users->get($this->request->data['from_user']);
                    //$student = $this->Users->get($usr['id']);

                    $html = "Vous avez un nouveau message de la part de <b>".$usr['lastname']." ".$usr['firstname']."</b> (<a href='mailto:".$usr['email']."'>".$usr['email']."</a>)<br/><br/><pre>".$this->request->data['message']."</pre>";

                    $email = new Email('default');
                    $email->from(['info@travail-pratique.com' => 'Travail-Pratique'])
                        ->emailFormat('html')
                        ->to($teacher->email)
                        ->subject('Travail-Pratique: Vous avez un nouveau message');
                        if($email->send($html)){
                            $status = true;
                        }else{
                            $this->Messages->delete($msg);
                        }
                }
            }
        }
        $this->set(compact('status'));
        $this->set('_serialize', ['status']);
    }


    /**
     * Delete method
     *
     * @param string|null $id Message id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $message = $this->Messages->get($id);
        if ($this->Messages->delete($message)) {
            $this->Flash->success(__('The message has been deleted.'));
        } else {
            $this->Flash->error(__('The message could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
