<?php
namespace App\Controller\Api;

use App\Controller\Api\AppController;
use Cake\Event\Event;

/**
 * Groups Controller
 *
 * @property \App\Model\Table\GroupsTable $Groups
 */
class GroupsController extends AppController
{

    public function initialize()
    {
        parent::initialize();
        if($this->request->is('options')) {
            $this->response->statusCode(204);
            $this->response->send();
            die();
        }
        //$this->Auth->allow(['groupsByFiliere']);
    }

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        /*$this->paginate = [
            'contain' => ['Filieres']
        ];
        $groups = $this->paginate($this->Groups);*/
        $groups = $this->Groups->find();
        $this->set(compact('groups'));
        $this->set('_serialize', ['groups']);
    }

    public function groupsByFiliere($filiere_id, $year){
        /*$allGroups = ['A','B','C','D','E','F','G','H','I','J','K'];

        $groups = $this->Groups->find()
                               ->select(['letter'])
                               ->matching('Filieres', function($q) use($filiere_id){
                                    return $q->where(['Filieres.id'=>$filiere_id]);
                               })
                               ->matching('Users', function($qu){
                                    return $qu->where(['Users.id'=>$this->Auth->user()['id']]);
                               })
                               ->where(['Groups.year'=>$year, 'Groups.institution_id'=>$this->Auth->user()['institution_id']])
                               ->toArray();

        foreach ($groups as $key => $value) {
            if (false !== $k = array_search($value->letter, $allGroups)) {
                unset($allGroups[$k]);
            }
        }
        usort($allGroups, function($a, $b) {
            return $a <=> $b;
        });*/
        $this->loadModel("UsersGroups");
        $ids = $this->UsersGroups->find()->select(['group_id'])->where(['user_id'=>$this->Auth->user()['id']]);
        $allGroups = $this->Groups->find()
                               ->select(['id','letter'])
                               ->where(['filiere_id'=>$filiere_id, 
                                        'year'=>$year,
                                        'institution_id'=>$this->Auth->user()['institution_id']
                                ])
                               ->andWhere(function ($q) use($ids){
                                    return $q->notIn('id', $ids);
                                })
                               ->order(['letter'=>'ASC'])
                               ->toArray();
        $allGroups = array_values($allGroups);
        $this->set(compact('allGroups'));
        $this->set('_serialize', ['allGroups']);
    }

    /*public function existingGroupsByFiliere($filiere_id, $year){
        $allGroups = $this->Groups->find()
                               ->contain(['Filieres'])
                               ->where(['filiere_id'=>$filiere_id, 
                                        'year'=>$year,
                                        'institution_id'=>$this->Auth->user()['institution_id']
                                ])
                               ->toArray();

        $this->loadModel("Modules")                       ;
        $modules = $this->Modules->find()
                                 ->where(['filiere_id'=>$filiere_id,'year'=>$year])
                                 ->order(['name'=>'ASC']);

        $allGroups = array_values($allGroups);
        //array_multisort($allGroups, 'letter', SORT_ASC);
        usort($allGroups, function($a, $b) {
            return $a['letter'] <=> $b['letter'];
        });
        //sort($allGroups);
        $this->set(compact('allGroups','modules'));
        $this->set('_serialize', ['allGroups','modules']);
    }*/
    
    public function existingGroupsByFiliere /*teachersGroupsByFiliere*/($filiere_id, $year){
        $this->loadModel("UsersGroups");
        $allGroups = [];
        $groups = $this->UsersGroups->find()
                                    ->matching('Groups',function($q) use($filiere_id, $year){
                                        $q->contain(['Filieres']);
                                        return $q->where(['filiere_id'=>$filiere_id, 'year'=>$year])->order(['letter'=>'ASC']);
                                    })
                                    /*->contain(['Groups'=>function($qr) use($filiere_id, $year){

                                        return $qr->where(['filiere_id'=>$filiere_id, 'year'=>$year]);
                                    }])*/
                                    ->where(['user_id'=>$this->Auth->user()['id']]);

        foreach ($groups as $group) {
            //print_r($group); die();
            array_push($allGroups, ['group_id'=>$group->group_id,
                                    'year'=>$group->group->year,
                                    'filiere'=>$group->group->filiere->shortname,
                                    'letter'=>$group->group->letter]);
        }

        usort($allGroups, function($a, $b) {
            return $a['letter'] <=> $b['letter'];
        });
        //sort($allGroups);
        //print_r(groups)
        $this->loadModel("Modules")                       ;
        $modules = $this->Modules->find()
                                 ->where(['filiere_id'=>$filiere_id,'year'=>$year])
                                 ->order(['name'=>'ASC']);

        $this->set(compact('allGroups','modules'));
        $this->set('_serialize', ['allGroups','modules']);
    }

    /**
     * View method
     *
     * @param string|null $id Group id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $group = $this->Groups->get($id, [
            'contain' => ['Filieres', 'Users']
        ]);

        $this->set('group', $group);
        $this->set('_serialize', ['group']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add(){
        //print_r($this->request->data); die();
        $this->loadModel("UsersGroups");
        $status = false;
        if ($this->request->is('post')) {
            $grp = $this->UsersGroups->find()->where(['user_id'=>$this->Auth->user()['id'],
                                                      'group_id'=>$this->request->data['group_id']])->toArray();
            if(!empty($grp)){
                $status = 'exists';
            }else{
                $usrGrpAssoc = $this->UsersGroups->newEntity();
                $usrGrpAssoc = $this->UsersGroups->patchEntity($usrGrpAssoc, [
                                                                'user_id'=>$this->Auth->user()['id'],
                                                                'group_id'=>$this->request->data['group_id']
                                                            ]);
                /*
                //Add ExtraData BelongsToMany data and institution_id to the array that will be saved
                $this->request->data['users'][0] = ['id'=>$this->Auth->user()['id']];
                $this->request->data['institution_id'] = $this->Auth->user()['institution_id'];

                //Saving data
                $group = $this->Groups->newEntity();
                $group = $this->Groups->patchEntity($group, $this->request->data);
                */
                if ($this->UsersGroups->save($usrGrpAssoc)) {
                    $status = true;
                }
            }
        }

        $this->set(compact('status'));
        $this->set('_serialize', ['status']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Group id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $group = $this->Groups->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $group = $this->Groups->patchEntity($group, $this->request->data);
            if ($this->Groups->save($group)) {
                $this->Flash->success(__('The group has been saved.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The group could not be saved. Please, try again.'));
            }
        }
        $filieres = $this->Groups->Filieres->find('list', ['limit' => 200]);
        $this->set(compact('group', 'filieres'));
        $this->set('_serialize', ['group']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Group id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $status = false;
        $this->loadModel("UsersGroups");
        $groupAssoc = $this->UsersGroups->find()
                                        ->where(['user_id'=>$this->Auth->user()['id'], 'group_id'=>$id])
                                        ->first();
        if ($this->UsersGroups->delete($groupAssoc)) {
            $status = true;
        }
        $this->set(compact('status'));
        $this->set('_serialize', ['status']);
    }

    public function getTeachersGroups(){
        $this->loadModel("Filieres");
        //gettin authenticated user id
        $user_id = $this->Auth->user()['id'];

        $filieres = $this->Filieres->find()
                                   ->contain(['Groups'=>function($qr) use($user_id){
                                            return $qr->matching('Users', function($q) use($user_id){
                                                return $q->where(['Users.id'=>$user_id]);
                                            })
                                            ->contain([
                                                'Tps'=>[
                                                    'sort'=>['deadline'=>'DESC']
                                                ],
                                                'Modules'=>[
                                                    'sort'=>['shortname'=>'DESC']
                                                ]
                                            ])
                                            ->order(['letter'=>'ASC']);
                                        }]);
        $this->set(compact('filieres'));
        $this->set('_serialize', ['filieres']);
    }

    public function getTeachersGroupsNoTPs(){
        $this->loadModel("Filieres");
        $user = $this->Auth->user();
        $filieres = $this->Filieres->find()
                                   ->matching('Institutions', function($q) use($user){
                                        return $q->where(['Institutions.id'=>$user['institution_id']]);
                                    })
                                   ->contain(['Groups'=>function($qr) use($user){
                                            return $qr->matching('Users', function($q) use($user){
                                                return $q->where(['Users.id'=>$user['id']]);
                                            })->order(['letter'=>'ASC']);
                                        }]);
        $this->set(compact('filieres'));
        $this->set('_serialize', ['filieres']);
    }
}
