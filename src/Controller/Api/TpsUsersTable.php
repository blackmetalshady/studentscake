<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * TpsUsers Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Tps
 * @property \Cake\ORM\Association\BelongsTo $Users
 *
 * @method \App\Model\Entity\TpsUser get($primaryKey, $options = [])
 * @method \App\Model\Entity\TpsUser newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\TpsUser[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\TpsUser|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\TpsUser patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\TpsUser[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\TpsUser findOrCreate($search, callable $callback = null)
 */
class TpsUsersTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('tps_users');
        $this->displayField('user_id');
        $this->primaryKey(['user_id', 'tp_id']);

        $this->belongsTo('Tps', [
            'foreignKey' => 'tp_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Users', [
            'foreignKey' => 'user_id',
            'joinType' => 'INNER'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {

        $validator
            ->uuid('user_id')
            ->requirePresence('user_id', 'create')
            ->notEmpty('user_id');

        $validator
            ->uuid('tp_id')
            ->requirePresence('tp_id', 'create')
            ->notEmpty('tp_id');

        $validator
            ->requirePresence('filename', 'create')
            ->notEmpty('filename');

        $validator
            ->integer('uploaded')
            ->requirePresence('uploaded', 'create')
            ->notEmpty('uploaded');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['tp_id'], 'Tps'));
        $rules->add($rules->existsIn(['user_id'], 'Users'));

        return $rules;
    }
}
