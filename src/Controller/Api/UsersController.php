<?php
namespace App\Controller\Api;

use App\Controller\Api\AppController;
use Cake\Event\Event;
use Cake\Network\Exception\UnauthorizedException;
use Cake\Utility\Security;
use Firebase\JWT\JWT;

/**
 * Users Controller
 *
 * @property \App\Model\Table\UsersTable $Users
 */
class UsersController extends AppController
{
    public function beforeFilter(Event $event)
    {
        parent::beforeFilter($event);
    }

    public function initialize()
    {
        parent::initialize();
        if($this->request->is('options')) {
            $this->response->statusCode(204);
            $this->response->send();
            die();
        }
        $this->Auth->allow(['add', 'token', 'exists','verify']);
    }

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $users = $this->paginate($this->Users);

        $this->set(compact('users'));
        $this->set('_serialize', ['users']);
    }

    /**
     * View method
     *
     * @param string|null $id User id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $user = $this->Users->get($id, [
            'contain' => []
        ]);

        $this->set('user', $user);
        $this->set('_serialize', ['user']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        if ($this->request->is('post')) {
            //get and add user Role to the request to be saved
            $this->loadModel("Roles");
            $role = $this->Roles->find()->where(['name'=>$this->request->data['role']])->first();
            $this->request->data['role_id'] = $role->id;
            //print_r(expression)
            $this->Crud->on('afterSave', function(Event $event) {
                if ($event->subject->created) {
                    $user = $this->Users->find()
                                        ->where(['email'=>$this->request->data['email']])
                                        ->first();

                    //after saving the user, save whether he's a student or a teacher
                    $modelname = ucfirst($this->request->data['role']).'s';
                    $this->loadModel("{$modelname}");
                    $this->request->data['user_id'] = $user->id;

                    $academic = $this->{$modelname}->newEntity();
                    $academic = $this->{$modelname}->patchEntity($academic, $this->request->data);
                    if($this->{$modelname}->save($academic)){
                        //genrate a token
                        $token = JWT::encode(
                                        [
                                            'sub' => $event->subject->entity->id,
                                            'exp' =>  time() + 604800
                                        ],
                                    Security::salt()
                                );
                        
                        //data to be sent to the client
                        $this->set('data', [
                            'id' => $event->subject->entity->id,
                            'token'=>$token
                        ]);

                        //generate and save a registration token
                        $user = $this->Users->patchEntity($user, ['registrationtoken'=>$token]);
                        if($user = $this->Users->save($user)){
                            // Send verification email
                            $this->sendverifyemail($user, $token);
                        }
                    }
                    
                    $this->Crud->action()->config('serialize.data', 'data');
                }
            });
            return $this->Crud->execute();
        }
    }

    public function token()
    {
        $user = $this->Auth->identify();

        if (!$user) {
            $this->set([
                'success'=>false, 
                'message'=>'E-mail/Mot de passe Incorrecte',
                '_serialize' => ['success', 'message']
                ]);
        }else{
            if ($user['enabled'] == '0' || $user['confirmed'] == '0') {
                $this->set([
                'success'=>false, 
                'message'=> ($user['confirmed'] == '0')? 'veuillez confirmez votre inscription on cliquant sur le lien qui à été envoyer à votre adresse email' :'Votre compte est désactivé, veuillez contacter votre formateur',
                '_serialize' => ['success', 'message']
                ]);
            }else{
                $user = $this->Users->find()->where(['Users.id'=>$user['id']])
                                            ->contain(['Roles', 'Students', 'Teachers'])
                                            ->first();

                $this->set([
                    'success' => true,
                    'data' => [
                        'token' => JWT::encode([
                            'sub' => $user['id'],
                            'exp' =>  time() + 604800
                        ],
                        Security::salt()),
                        'user'=>$user
                    ],
                    '_serialize' => ['success', 'data']
                ]);
            }
        }  
    }

    public function verify(){
        if ($this->request->is('post')) {
            $user = $this->Users->find()
                            ->where(['registrationtoken'=>$this->request->data['token']])
                            ->first();
            if (empty($user) || $user->id !== $this->request->data['user_id'] || $user->confirmed == '1') {
                $data = ['status'=>'404','success'=>'false','msg'=>'Page Introuvable'];
            }else{
                $user = $this->Users->patchEntity($user,['confirmed'=>'1','enabled'=>'1']);
                if ($this->Users->save($user)) {
                    $data = ['status'=>'200','success'=>'true','msg'=>'Votre compte a été activé avec succès'];
                }else{
                    $data = ['status'=>'500','success'=>'false','msg'=>'Une Erreur est survenu, Veuillez réessayer plutard'];
                }
            }
        }
        

        $this->set('data', $data);
        $this->set('_serialize', ['data']);
    }

    /**
     * Edit method
     *
     * @param string|null $id User id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $success = false;
        $user = $this->Users->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $user = $this->Users->patchEntity($user, $this->request->data);
            if ($this->Users->save($user)) {
                $success = true;
            }
        }
        $this->set(compact('success'));
        $this->set('_serialize', ['success']);
    }

    /**
     * Delete method
     *
     * @param string|null $id User id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $status = false;
        $user = $this->Users->get($id);
        if ($this->Users->delete($user)) {
            $status = true;
        }
        $this->set(compact('status'));
        $this->set('_serialize', ['status']);
    }

    public function userdata(){

        
        if ($usr = $this->Auth->user()) {
            $data = ['status'=>true,'message'=>'User is Logged', 'user'=>$usr];
        }else{
            $data = ['status'=>false,'message'=>'User is NOT Logged'];
        }

        $this->set(compact('data'));
        $this->set('_serialize', ['data']);
    }

    public function logout()
    {
        if($this->Auth->logout()){
            $data = ['status'=>true,'message'=>'ok'];
        }else{
            $data = ['status'=>false,'message'=>'Erreur'];
        }

        $this->set(compact('data'));
        $this->set('_serialize', ['data']);
    }

    public function taineesByGroup($group_id = null){
        $trainees = $this->Users->find()

                                ->matching('Groups', function ($q) use($group_id){
                                    return $q->where(['Groups.id' => $group_id]);
                                })
                                ->matching('Roles', function ($q) {
                                    return $q->where(['Roles.name' => 'student']);
                                });
        $this->set(compact('trainees'));
        $this->set('_serialize', ['trainees']);
    }

    public function exists(){
        $exists = false;
        $existingEmail = "";
        if($this->request->is('post')){
            $account = $this->Users->find()
                                   ->where(['email'=>$this->request->data['email']])
                                   ->first();
            if(!empty($account)){
                $exists = true;
                $existingEmail = $account->email;
            }
        }
        
        $result = ['exists'=>$exists, 'email'=>$existingEmail];
        $this->set(compact('result'));
        $this->set('_serialize', ['result']);
    }


    public function getTeachersByGroup(){
        if ($this->request->is('post')) {
            $this->loadModel('Roles');
            //$teacher_role = $this->Roles->find()->where(['name'=>'teacher'])->first();
            $teachers = $this->Users->find()
                                    ->select(['Users.id', 'Users.firstname','Users.lastname','Users.email'])
                                    ->matching('Roles', function ($q) {
                                        return $q->where(['Roles.name' => 'teacher']);
                                      })
                                    //->where(['role_id'=>$teacher_role->id])
                                    ->contain(['Teachers'=>function ($q){
                                       return $q->select(['Teachers.id'])
                                                ->contain(['Modules'=>function ($q){
                                                    return $q->select(['Modules.id','Modules.name']);
                                                }]);
                                            },
                                    'Groups'=>function ($q){
                                       return $q
                                            ->where(['Groups.id' => $this->request->data['group_id']]);
                                    }]);
        }
        
        $this->set(compact('teachers'));
        $this->set('_serialize', ['teachers']);
    }
}