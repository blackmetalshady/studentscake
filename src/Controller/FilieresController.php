<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Filieres Controller
 *
 * @property \App\Model\Table\FilieresTable $Filieres
 */
class FilieresController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $filieres = $this->paginate($this->Filieres);

        $this->set(compact('filieres'));
        $this->set('_serialize', ['filieres']);
    }

    /**
     * View method
     *
     * @param string|null $id Filiere id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $filiere = $this->Filieres->get($id, [
            'contain' => ['Groups']
        ]);

        $this->set('filiere', $filiere);
        $this->set('_serialize', ['filiere']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $filiere = $this->Filieres->newEntity();
        if ($this->request->is('post')) {
            $filiere = $this->Filieres->patchEntity($filiere, $this->request->data);
            if ($this->Filieres->save($filiere)) {
                $this->Flash->success(__('The filiere has been saved.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The filiere could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('filiere'));
        $this->set('_serialize', ['filiere']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Filiere id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $filiere = $this->Filieres->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $filiere = $this->Filieres->patchEntity($filiere, $this->request->data);
            if ($this->Filieres->save($filiere)) {
                $this->Flash->success(__('The filiere has been saved.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The filiere could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('filiere'));
        $this->set('_serialize', ['filiere']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Filiere id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $filiere = $this->Filieres->get($id);
        if ($this->Filieres->delete($filiere)) {
            $this->Flash->success(__('The filiere has been deleted.'));
        } else {
            $this->Flash->error(__('The filiere could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
