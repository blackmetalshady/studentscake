-- Adminer 4.2.4 MySQL dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

DROP DATABASE IF EXISTS `students`;
CREATE DATABASE `students` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `students`;

DROP TABLE IF EXISTS `blacklist`;
CREATE TABLE `blacklist` (
  `id` char(36) NOT NULL,
  `email` varchar(100) NOT NULL,
  `user_id` char(36) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `email` (`email`),
  KEY `user_id` (`user_id`),
  CONSTRAINT `blacklist_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `cities`;
CREATE TABLE `cities` (
  `id` char(36) NOT NULL,
  `name` varchar(255) NOT NULL,
  `zipcode` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `cities` (`id`, `name`, `zipcode`) VALUES
('003cba5d-488b-4d33-8443-e827bd78d5a1',	'Casablanca',	20000),
('003cba5d-488b-4d33-8443-e827bd78d5a2',	'Marrakech',	40000),
('003cba5d-488b-4d33-8443-e827bd78d5a3',	'Rabat',	10000);

DROP TABLE IF EXISTS `filieres`;
CREATE TABLE `filieres` (
  `id` char(36) NOT NULL,
  `name` varchar(255) NOT NULL,
  `shortname` varchar(10) NOT NULL,
  `years_of_study` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `filieres` (`id`, `name`, `shortname`, `years_of_study`) VALUES
('56b35ea9-fd9f-494e-805b-645066f6668',	'Technicien de Maintenance, Support Informatique et Reseau',	'TMSIR',	2),
('76b35ead5-f69f-424e-805b-6450235f68',	'Techniques de Developpement Informatique ',	'TDI',	2),
('76b35ead5-f69f-424e-805b-645025f669',	'Gros Oeuvres',	'TSGO',	2),
('96b35ea9-f69f-404e-805b-6450665f668',	'Techniques des Reseaux Informatique',	'TRI',	2);

DROP TABLE IF EXISTS `filieres_institutions`;
CREATE TABLE `filieres_institutions` (
  `filiere_id` char(36) NOT NULL,
  `institution_id` char(36) NOT NULL,
  KEY `filiere_id` (`filiere_id`),
  KEY `institution_id` (`institution_id`),
  CONSTRAINT `filieres_institutions_ibfk_3` FOREIGN KEY (`filiere_id`) REFERENCES `filieres` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `filieres_institutions_ibfk_4` FOREIGN KEY (`institution_id`) REFERENCES `institutions` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `filieres_institutions` (`filiere_id`, `institution_id`) VALUES
('56b35ea9-fd9f-494e-805b-645066f6668',	'004cba5d-488b-4d33-8443-e827bd78d5a3'),
('96b35ea9-f69f-404e-805b-6450665f668',	'004cba5d-488b-4d33-8443-e827bd78d5a3'),
('76b35ead5-f69f-424e-805b-6450235f68',	'004cba5d-488b-4d33-8443-e827bd78d5a3'),
('76b35ead5-f69f-424e-805b-645025f669',	'004cba5d-488b-4d33-8443-e827bd78d5a2');

DROP TABLE IF EXISTS `groups`;
CREATE TABLE `groups` (
  `id` char(36) NOT NULL,
  `year` int(11) NOT NULL,
  `letter` varchar(10) NOT NULL,
  `filiere_id` char(36) NOT NULL,
  `institution_id` char(36) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `id` (`id`),
  KEY `filiere_id` (`filiere_id`),
  KEY `institution_id` (`institution_id`),
  CONSTRAINT `groups_ibfk_1` FOREIGN KEY (`filiere_id`) REFERENCES `filieres` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `groups_ibfk_3` FOREIGN KEY (`institution_id`) REFERENCES `institutions` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `groups` (`id`, `year`, `letter`, `filiere_id`, `institution_id`) VALUES
('067105d-de39-4d13-8949-b0840a75826',	2,	'D',	'56b35ea9-fd9f-494e-805b-645066f6668',	'004cba5d-488b-4d33-8443-e827bd78d5a3'),
('5cd67264-d514-4416-ba2a-059bbcaf35c',	1,	'A',	'56b35ea9-fd9f-494e-805b-645066f6668',	'004cba5d-488b-4d33-8443-e827bd78d5a3'),
('6ab563eb-5178-440e-8fcb-1f0d0185442',	1,	'A',	'96b35ea9-f69f-404e-805b-6450665f668',	'004cba5d-488b-4d33-8443-e827bd78d5a3'),
('6de327c9-cae3-11e7-b26c-3417ebe21fc',	1,	'C',	'96b35ea9-f69f-404e-805b-6450665f668',	'004cba5d-488b-4d33-8443-e827bd78d5a3'),
('6de34d8e-cae3-11e7-b26c-3417ebe21fe',	1,	'D',	'96b35ea9-f69f-404e-805b-6450665f668',	'004cba5d-488b-4d33-8443-e827bd78d5a3'),
('79e08051-2133-4a59-92c7-6cf10253e58',	2,	'A',	'96b35ea9-f69f-404e-805b-6450665f668',	'004cba5d-488b-4d33-8443-e827bd78d5a3'),
('8daf6fc7-fe52-4dc0-8546-81b3bc4d8989',	1,	'C',	'56b35ea9-fd9f-494e-805b-645066f6668',	'004cba5d-488b-4d33-8443-e827bd78d5a3'),
('9ae3a4c4-74bc-447e-8583-8e78b50fe2a',	1,	'D',	'56b35ea9-fd9f-494e-805b-645066f6668',	'004cba5d-488b-4d33-8443-e827bd78d5a3'),
('9b13f171-80c5-44ad-971d-6d0037e2250',	1,	'B',	'96b35ea9-f69f-404e-805b-6450665f668',	'004cba5d-488b-4d33-8443-e827bd78d5a3'),
('9ece07e0-9587-4bc4-bcce-e3627373bf5',	1,	'B',	'76b35ead5-f69f-424e-805b-6450235f68',	'004cba5d-488b-4d33-8443-e827bd78d5a3'),
('9f6247c3-f1c3-484c-8239-859485ab364',	1,	'A',	'76b35ead5-f69f-424e-805b-6450235f68',	'004cba5d-488b-4d33-8443-e827bd78d5a3'),
('bd67be72-cae3-11e7-b26c-341ebe721d3',	1,	'E',	'96b35ea9-f69f-404e-805b-6450665f668',	'004cba5d-488b-4d33-8443-e827bd78d5a3'),
('bd67d999-cae3-11e7-b26c-3417ebe21fe',	1,	'F',	'96b35ea9-f69f-404e-805b-6450665f668',	'004cba5d-488b-4d33-8443-e827bd78d5a3'),
('c7220616-add3-4c8f-acd3-1a0124623c0',	1,	'B',	'56b35ea9-fd9f-494e-805b-645066f6668',	'004cba5d-488b-4d33-8443-e827bd78d5a3');

DROP TABLE IF EXISTS `institutions`;
CREATE TABLE `institutions` (
  `id` char(36) NOT NULL,
  `fullname` varchar(255) NOT NULL,
  `shortname` varchar(50) NOT NULL,
  `city_id` char(36) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `city_id` (`city_id`),
  CONSTRAINT `institutions_ibfk_1` FOREIGN KEY (`city_id`) REFERENCES `cities` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `institutions` (`id`, `fullname`, `shortname`, `city_id`) VALUES
('004cba5d-488b-4d33-8443-e827bd78d5a1',	'Institut supérieur de Gestion et d\'Informatique',	'ISGI',	'003cba5d-488b-4d33-8443-e827bd78d5a2'),
('004cba5d-488b-4d33-8443-e827bd78d5a2',	'Institut spécialisé Industriel de Marrakech',	'ISIM',	'003cba5d-488b-4d33-8443-e827bd78d5a2'),
('004cba5d-488b-4d33-8443-e827bd78d5a3',	'NTIC Sidi Youssef Ben Ali',	'NTIC S.Y.B.A',	'003cba5d-488b-4d33-8443-e827bd78d5a2');

DROP TABLE IF EXISTS `materials`;
CREATE TABLE `materials` (
  `id` char(36) NOT NULL,
  `title` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `filename` varchar(255) DEFAULT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `module_id` char(36) CHARACTER SET latin1 NOT NULL,
  PRIMARY KEY (`id`),
  KEY `module_id` (`module_id`),
  CONSTRAINT `materials_ibfk_2` FOREIGN KEY (`module_id`) REFERENCES `modules` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `messages`;
CREATE TABLE `messages` (
  `id` char(36) NOT NULL,
  `from_user` char(36) NOT NULL,
  `to_user` char(36) NOT NULL,
  `module_id` char(36) CHARACTER SET latin1 NOT NULL,
  `message` text CHARACTER SET latin1 NOT NULL,
  `sent_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `read` tinyint(4) NOT NULL DEFAULT '0',
  KEY `module_id` (`module_id`),
  KEY `to_user` (`to_user`),
  KEY `from_user` (`from_user`),
  CONSTRAINT `messages_ibfk_6` FOREIGN KEY (`module_id`) REFERENCES `modules` (`id`) ON UPDATE CASCADE,
  CONSTRAINT `messages_ibfk_7` FOREIGN KEY (`to_user`) REFERENCES `users` (`id`) ON UPDATE CASCADE,
  CONSTRAINT `messages_ibfk_8` FOREIGN KEY (`from_user`) REFERENCES `users` (`id`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `messages` (`id`, `from_user`, `to_user`, `module_id`, `message`, `sent_at`, `read`) VALUES
('33250624-411e-4c32-8575-ffa7fe14038e',	'5ae2b81d-15d7-4697-b9c2-776077873a74',	'a87af55b-a7ba-4efa-989a-ab0cced8382f',	'5cd67264-d514-4416-ba2a-059bbbcaf35',	'heloooo',	'2017-11-19 20:56:22',	0),
('bd535be8-aa85-4591-88d3-ab73bda40568',	'5ae2b81d-15d7-4697-b9c2-776077873a74',	'a87af55b-a7ba-4efa-989a-ab0cced8382f',	'5cd67264-d514-4416-ba2a-059bbbcaf35',	'test',	'2017-11-19 21:10:32',	0),
('02badada-7d74-43a2-90e9-b743e32f3f45',	'5ae2b81d-15d7-4697-b9c2-776077873a74',	'a87af55b-a7ba-4efa-989a-ab0cced8382f',	'5cd67264-d514-4416-ba2a-059bbbcaf35',	'test',	'2017-11-19 21:11:56',	0),
('a2384a39-bd0b-498f-83ac-18a921dee423',	'5ae2b81d-15d7-4697-b9c2-776077873a74',	'a87af55b-a7ba-4efa-989a-ab0cced8382f',	'5cd67264-d514-4416-ba2a-059bbbcaf35',	'test',	'2017-11-19 21:15:22',	0),
('b9f3f3fe-6bc2-407c-8aad-17261e2ea216',	'5ae2b81d-15d7-4697-b9c2-776077873a74',	'a87af55b-a7ba-4efa-989a-ab0cced8382f',	'26b9c5f4-4bb2-48f7-9e48-ef3824a048d1',	'test test',	'2017-11-19 21:22:30',	0),
('2df5e7fd-5403-4d35-8975-eba2d8d2723e',	'5ae2b81d-15d7-4697-b9c2-776077873a74',	'a87af55b-a7ba-4efa-989a-ab0cced8382f',	'5cd67264-d514-4416-ba2a-059bbbcaf35',	'Ce message et pour toi mon cher professeur',	'2017-11-19 21:25:57',	0),
('d4657b47-3aea-4be3-b047-683714aee888',	'5ae2b81d-15d7-4697-b9c2-776077873a74',	'a87af55b-a7ba-4efa-989a-ab0cced8382f',	'5cd67264-d514-4416-ba2a-059bbbcaf35',	'hello teacher',	'2017-11-19 21:36:27',	0),
('cb9b427f-9726-46b9-992f-3f7f38e17445',	'5ae2b81d-15d7-4697-b9c2-776077873a74',	'a87af55b-a7ba-4efa-989a-ab0cced8382f',	'5cd67264-d514-4416-ba2a-059bbbcaf35',	'test TLS/SSL',	'2017-11-19 21:47:52',	0),
('6fc98b9f-6c6d-4d50-8eef-b18c9e0f17eb',	'5ae2b81d-15d7-4697-b9c2-776077873a74',	'a87af55b-a7ba-4efa-989a-ab0cced8382f',	'5cd67264-d514-4416-ba2a-059bbbcaf35',	'test TLS/SSL',	'2017-11-19 21:50:48',	0),
('3b1c20c2-44fa-4cae-8f78-25e27a53a306',	'5ae2b81d-15d7-4697-b9c2-776077873a74',	'a87af55b-a7ba-4efa-989a-ab0cced8382f',	'5cd67264-d514-4416-ba2a-059bbbcaf35',	'Sans  TLS',	'2017-11-19 21:53:31',	0),
('0624bee1-097f-427d-a7bd-41c176f6090a',	'5ae2b81d-15d7-4697-b9c2-776077873a74',	'a87af55b-a7ba-4efa-989a-ab0cced8382f',	'5cd67264-d514-4416-ba2a-059bbbcaf35',	'Sans  TLS',	'2017-11-19 21:54:10',	0);

DROP TABLE IF EXISTS `modules`;
CREATE TABLE `modules` (
  `id` char(36) NOT NULL,
  `name` varchar(255) NOT NULL,
  `shortname` varchar(10) NOT NULL,
  `filiere_id` char(36) CHARACTER SET utf8 NOT NULL,
  `year` tinyint(4) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `filiere_id` (`filiere_id`),
  CONSTRAINT `modules_ibfk_2` FOREIGN KEY (`filiere_id`) REFERENCES `filieres` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `modules` (`id`, `name`, `shortname`, `filiere_id`, `year`) VALUES
('26b9c5f4-4bb2-48f7-9e48-ef3824a048d1',	'Systeme d\'Exploitation Open Source',	'SEOS',	'56b35ea9-fd9f-494e-805b-645066f6668',	1),
('4d7d3496-cae4-11e7-b26c-3417ebe21fe',	'Essentiel en Technologie de l\'information',	'ETI',	'96b35ea9-f69f-404e-805b-6450665f668',	1),
('5cd67264-d514-4416-ba2a-059bbbcaf35',	'Diagnostique et Maintenance d\'un Poste Informatique',	'DMPI',	'56b35ea9-fd9f-494e-805b-645066f6668',	1);

DROP TABLE IF EXISTS `modules_groups`;
CREATE TABLE `modules_groups` (
  `module_id` char(36) NOT NULL,
  `group_id` char(36) CHARACTER SET utf8 NOT NULL,
  PRIMARY KEY (`module_id`,`group_id`),
  KEY `group_id` (`group_id`),
  CONSTRAINT `modules_groups_ibfk_4` FOREIGN KEY (`group_id`) REFERENCES `groups` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `modules_groups_ibfk_6` FOREIGN KEY (`module_id`) REFERENCES `modules` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `modules_groups` (`module_id`, `group_id`) VALUES
('5cd67264-d514-4416-ba2a-059bbbcaf35',	'5cd67264-d514-4416-ba2a-059bbcaf35c'),
('4d7d3496-cae4-11e7-b26c-3417ebe21fe',	'6de327c9-cae3-11e7-b26c-3417ebe21fc'),
('4d7d3496-cae4-11e7-b26c-3417ebe21fe',	'6de34d8e-cae3-11e7-b26c-3417ebe21fe'),
('5cd67264-d514-4416-ba2a-059bbbcaf35',	'8daf6fc7-fe52-4dc0-8546-81b3bc4d8989'),
('4d7d3496-cae4-11e7-b26c-3417ebe21fe',	'bd67be72-cae3-11e7-b26c-341ebe721d3'),
('5cd67264-d514-4416-ba2a-059bbbcaf35',	'c7220616-add3-4c8f-acd3-1a0124623c0');

DROP TABLE IF EXISTS `roles`;
CREATE TABLE `roles` (
  `id` char(36) NOT NULL,
  `name` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `roles` (`id`, `name`) VALUES
('104cbz5d-498b-4d33-8343-e827bd78d5b',	'student'),
('104cbz5d-498b-4d33-8343-e827bd78d5d',	'teacher'),
('104cbz5d-498b-4d33-8343-e827bd78dp',	'admin');

DROP TABLE IF EXISTS `students`;
CREATE TABLE `students` (
  `id` char(36) NOT NULL,
  `group_id` char(36) NOT NULL,
  `filiere_id` char(36) NOT NULL,
  `user_id` char(36) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `group_id` (`group_id`),
  KEY `filiere_id` (`filiere_id`),
  KEY `user_id` (`user_id`),
  CONSTRAINT `students_ibfk_3` FOREIGN KEY (`group_id`) REFERENCES `groups` (`id`) ON UPDATE CASCADE,
  CONSTRAINT `students_ibfk_4` FOREIGN KEY (`filiere_id`) REFERENCES `filieres` (`id`) ON UPDATE CASCADE,
  CONSTRAINT `students_ibfk_6` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `students` (`id`, `group_id`, `filiere_id`, `user_id`) VALUES
('5ae2b81d-15d7-4697-b9c2-776077873a74',	'5cd67264-d514-4416-ba2a-059bbcaf35c',	'56b35ea9-fd9f-494e-805b-645066f6668',	'5ae2b81d-15d7-4697-b9c2-776077873a74'),
('c8966705-af8a-4235-85c7-eaaeb256d55f',	'5cd67264-d514-4416-ba2a-059bbcaf35c',	'56b35ea9-fd9f-494e-805b-645066f6668',	'9cb4491b-6140-4d48-924f-52fed39902ef');

DROP TABLE IF EXISTS `teachers`;
CREATE TABLE `teachers` (
  `id` char(36) NOT NULL,
  `ppr` int(11) NOT NULL,
  `user_id` char(36) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  CONSTRAINT `teachers_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `teachers` (`id`, `ppr`, `user_id`) VALUES
('104cbz5d-498b-4d33-8343-e827bd78d5a',	1611130,	'a87af55b-a7ba-4efa-989a-ab0cced8382f'),
('cf792f61-3325-4520-9c55-7e9b536551f2',	159632,	'a1d82d73-a501-4dd2-8273-45a941bfa19b');

DROP TABLE IF EXISTS `teachers_modules`;
CREATE TABLE `teachers_modules` (
  `id` char(36) NOT NULL,
  `teacher_id` char(36) NOT NULL,
  `module_id` char(36) CHARACTER SET latin1 NOT NULL,
  PRIMARY KEY (`id`),
  KEY `teacher_id` (`teacher_id`),
  KEY `module_id` (`module_id`),
  CONSTRAINT `teachers_modules_ibfk_3` FOREIGN KEY (`teacher_id`) REFERENCES `teachers` (`id`) ON UPDATE CASCADE,
  CONSTRAINT `teachers_modules_ibfk_4` FOREIGN KEY (`module_id`) REFERENCES `modules` (`id`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `teachers_modules` (`id`, `teacher_id`, `module_id`) VALUES
('504cbz5d-498b-4d33-8343-e827bd78d76',	'104cbz5d-498b-4d33-8343-e827bd78d5a',	'26b9c5f4-4bb2-48f7-9e48-ef3824a048d1'),
('99b9c5f4-4bb2-48f7-9e48-ef3824a048d7',	'104cbz5d-498b-4d33-8343-e827bd78d5a',	'5cd67264-d514-4416-ba2a-059bbbcaf35');

DROP TABLE IF EXISTS `tps`;
CREATE TABLE `tps` (
  `id` char(36) NOT NULL,
  `title` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `created_at` datetime NOT NULL,
  `deadline` datetime NOT NULL,
  `enonce` tinyint(4) NOT NULL DEFAULT '0',
  `filename` varchar(255) DEFAULT NULL,
  `module_id` char(36) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `module_id` (`module_id`),
  CONSTRAINT `tps_ibfk_2` FOREIGN KEY (`module_id`) REFERENCES `modules` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `tps` (`id`, `title`, `description`, `created_at`, `deadline`, `enonce`, `filename`, `module_id`) VALUES
('1760d123-b2af-49b8-af8d-0e02ba6aaedc',	'TP 5',	'hello',	'2017-11-06 21:50:19',	'2017-11-19 22:22:00',	1,	'1760d123-b2af-49b8-af8d-0e02ba6aaedc.ppt',	'5cd67264-d514-4416-ba2a-059bbbcaf35'),
('4708b79f-5f82-46af-ab66-72bc0df74134',	'TP 888',	'gtztgzert',	'2017-01-23 16:10:13',	'2017-01-28 05:00:00',	0,	NULL,	'5cd67264-d514-4416-ba2a-059bbbcaf35'),
('65bbcfe7-d2a4-41a0-842d-f0420e7c3ec9',	'TP2 - Commandes find, grep et wc',	'Differents cas d\'utilisation de la commande find, grep et wc, avec combinaison des commande en utilisant les pipes, utilisez le help ou la commande \"man\" pour trouver les bons paramètres à utiliser.',	'0000-00-00 00:00:00',	'2017-04-11 23:59:00',	1,	'65bbcfe7-d2a4-41a0-842d-f0420e7c3ec9.pdf',	'26b9c5f4-4bb2-48f7-9e48-ef3824a048d1'),
('6fc9ee36-2bba-40e3-b026-2028982103fd',	'TP1 - Commande de Base Linux',	'Le TP 1 vise a se familiariser avec les commandes de base de Linux, la manipulation peut être faite sous Ubuntu ou Centos, l\'utilisation de la commande man est recommandé pour trouver la bonne syntaxe de chaque commande.',	'0000-00-00 00:00:00',	'2017-04-06 22:59:00',	1,	'6fc9ee36-2bba-40e3-b026-2028982103fd.pdf',	'26b9c5f4-4bb2-48f7-9e48-ef3824a048d1'),
('87710d5b-f9e2-479e-b79a-59055f9eb89e',	'TP2 - Commande Find, Grep et WC',	'Differents cas d\'utilisation de la commande find, grep et wc, avec combinaison des commande en utilisant les pipes, utilisez le help ou la commande \"man\" pour trouver les bons paramètres à utiliser.',	'0000-00-00 00:00:00',	'2017-05-04 23:59:00',	1,	'87710d5b-f9e2-479e-b79a-59055f9eb89e.pdf',	'26b9c5f4-4bb2-48f7-9e48-ef3824a048d1'),
('bc53a7ae-177a-4f44-a3e1-a360ffa7389e',	'azeaz',	'azraerazer',	'2017-01-23 15:52:16',	'2017-01-28 05:55:00',	0,	NULL,	'5cd67264-d514-4416-ba2a-059bbbcaf35'),
('c91451ae-c738-4894-8770-1c354d29ee44',	'TP2 - Variables D\'environnement',	'Vous devez cree un fichier .bat contenant les le code suivant : \r\n @echo ==Votre Nom et Prenom ==\r\n pause\r\naprès il faut mettre a jour la variable d\'environnement PATH pour que la commande soit reconnus par CMD',	'0000-00-00 00:00:00',	'2017-05-17 23:59:00',	0,	NULL,	'5cd67264-d514-4416-ba2a-059bbbcaf35'),
('d0a97674-a20e-48ee-b378-52c54f923b28',	'TP1 - Variables d\'Environnement',	'Vous devez cree un fichier .bat contenant les le code suivant : \r\n@echo ==TMSIR==\r\npause\r\naprès il faut mettre a jour la variable d\'environnement PATH pour que la commande soit reconnus par CMD',	'0000-00-00 00:00:00',	'2017-05-06 23:59:00',	0,	NULL,	'5cd67264-d514-4416-ba2a-059bbbcaf35'),
('e808b86e-4368-4856-a88b-8e0b634890ac',	'TP1 - Commande de Base Linux',	'Le TP 1 vise a se familiariser avec les commandes de base de Linux, la manipulation peut être faite sous Ubuntu ou Centos, l\'utilisation de la commande man est recommandé pour trouver la bonne syntaxe de chaque commande.',	'0000-00-00 00:00:00',	'2017-04-06 22:59:00',	1,	'e808b86e-4368-4856-a88b-8e0b634890ac.pdf',	'26b9c5f4-4bb2-48f7-9e48-ef3824a048d1');

DROP TABLE IF EXISTS `tps_groups`;
CREATE TABLE `tps_groups` (
  `group_id` char(36) CHARACTER SET utf8 NOT NULL,
  `tp_id` char(36) NOT NULL,
  `start_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`group_id`,`tp_id`),
  KEY `tp_id` (`tp_id`),
  CONSTRAINT `tps_groups_ibfk_3` FOREIGN KEY (`group_id`) REFERENCES `groups` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `tps_groups_ibfk_4` FOREIGN KEY (`tp_id`) REFERENCES `tps` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `tps_groups` (`group_id`, `tp_id`, `start_time`) VALUES
('5cd67264-d514-4416-ba2a-059bbcaf35c',	'1760d123-b2af-49b8-af8d-0e02ba6aaedc',	'2017-11-06 21:50:19'),
('5cd67264-d514-4416-ba2a-059bbcaf35c',	'65bbcfe7-d2a4-41a0-842d-f0420e7c3ec9',	'0000-00-00 00:00:00'),
('5cd67264-d514-4416-ba2a-059bbcaf35c',	'6fc9ee36-2bba-40e3-b026-2028982103fd',	'0000-00-00 00:00:00'),
('5cd67264-d514-4416-ba2a-059bbcaf35c',	'c91451ae-c738-4894-8770-1c354d29ee44',	'0000-00-00 00:00:00'),
('8daf6fc7-fe52-4dc0-8546-81b3bc4d8989',	'6fc9ee36-2bba-40e3-b026-2028982103fd',	'0000-00-00 00:00:00'),
('8daf6fc7-fe52-4dc0-8546-81b3bc4d8989',	'87710d5b-f9e2-479e-b79a-59055f9eb89e',	'0000-00-00 00:00:00'),
('c7220616-add3-4c8f-acd3-1a0124623c0',	'1760d123-b2af-49b8-af8d-0e02ba6aaedc',	'2017-11-06 21:50:19'),
('c7220616-add3-4c8f-acd3-1a0124623c0',	'65bbcfe7-d2a4-41a0-842d-f0420e7c3ec9',	'0000-00-00 00:00:00'),
('c7220616-add3-4c8f-acd3-1a0124623c0',	'd0a97674-a20e-48ee-b378-52c54f923b28',	'0000-00-00 00:00:00'),
('c7220616-add3-4c8f-acd3-1a0124623c0',	'e808b86e-4368-4856-a88b-8e0b634890ac',	'0000-00-00 00:00:00');

DROP TABLE IF EXISTS `tps_users`;
CREATE TABLE `tps_users` (
  `tp_id` char(36) NOT NULL,
  `user_id` char(36) CHARACTER SET utf8 NOT NULL,
  `filename` varchar(255) NOT NULL,
  `uploaded` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`user_id`,`tp_id`),
  KEY `tp_id` (`tp_id`),
  CONSTRAINT `tps_users_ibfk_3` FOREIGN KEY (`tp_id`) REFERENCES `tps` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `tps_users_ibfk_4` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `tps_users` (`tp_id`, `user_id`, `filename`, `uploaded`) VALUES
('1760d123-b2af-49b8-af8d-0e02ba6aaedc',	'5ae2b81d-15d7-4697-b9c2-776077873a74',	'5ae2b81d-15d7-4697-b9c2-776077873a74_1760d123-b2af-49b8-af8d-0e02ba6aaedc.doc',	1);

DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` char(36) NOT NULL,
  `role_id` char(36) NOT NULL DEFAULT '104cbz5d-498b-4d33-8343-e827bd78d5b',
  `firstname` varchar(45) NOT NULL,
  `lastname` varchar(45) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `created` datetime NOT NULL,
  `last_login` datetime DEFAULT NULL,
  `enabled` tinyint(4) NOT NULL DEFAULT '0',
  `confirmed` tinyint(4) NOT NULL DEFAULT '0',
  `registrationtoken` text,
  `institution_id` char(36) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `institution_id` (`institution_id`),
  KEY `role_id` (`role_id`),
  CONSTRAINT `users_ibfk_5` FOREIGN KEY (`institution_id`) REFERENCES `institutions` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `users_ibfk_8` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `users` (`id`, `role_id`, `firstname`, `lastname`, `email`, `password`, `created`, `last_login`, `enabled`, `confirmed`, `registrationtoken`, `institution_id`) VALUES
('5ae2b81d-15d7-4697-b9c2-776077873a74',	'104cbz5d-498b-4d33-8343-e827bd78d5b',	'Sabah',	'Aamir',	'yassine.elkhanboubi@gmail.com',	'$2y$10$/zrRU0yKnJWlAB2.C45IHehYxqzWakUqsiyXYZTDNXrJsnyP0hLrS',	'2017-06-09 12:10:22',	NULL,	1,	1,	'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiI1YWUyYjgxZC0xNWQ3LTQ2OTctYjljMi03NzYwNzc4NzNhNzQiLCJleHAiOjE0OTc2MTUwMjJ9.XXWIytoy-dD4cRNdtdsF08CgbyD-Cv2px5WgZhjCXs8',	'004cba5d-488b-4d33-8443-e827bd78d5a3'),
('9cb4491b-6140-4d48-924f-52fed39902ef',	'104cbz5d-498b-4d33-8343-e827bd78d5b',	'amin',	'khuyel',	'yas-yo@hotmail.com',	'$2y$10$I46M8xdfuD8tBsArNWBrWe8M5DaqO98EPameFNILVhTAOPgaooucC',	'2017-06-14 12:06:24',	NULL,	1,	1,	'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiI5Y2I0NDkxYi02MTQwLTRkNDgtOTI0Zi01MmZlZDM5OTAyZWYiLCJleHAiOjE0OTgwNDY3ODR9.kp1xEpeOHa44TVC6qyTciqO8MluGS6pU6WwtpFpSi1Y',	'004cba5d-488b-4d33-8443-e827bd78d5a3'),
('a1d82d73-a501-4dd2-8273-45a941bfa19b',	'104cbz5d-498b-4d33-8343-e827bd78d5d',	'Abdelhadi',	'Bouain',	'yas-yo2@hotmail.com',	'$2y$10$65lNBTPk1J15tXcuvOgsZufHvp9eyWylBMdTpnDot65VNX/nSDAj2',	'2017-06-19 13:11:56',	NULL,	1,	1,	'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJhMWQ4MmQ3My1hNTAxLTRkZDItODI3My00NWE5NDFiZmExOWIiLCJleHAiOjE0OTg0ODI3MTd9.JDQwNfA-clp4mV4gNN-n-9KbH_uwhWnt27jFppI_I_0',	'004cba5d-488b-4d33-8443-e827bd78d5a3'),
('a87af55b-a7ba-4efa-989a-ab0cced8382f',	'104cbz5d-498b-4d33-8343-e827bd78d5d',	'Yassine',	'El Khanboubi',	'elkhanboubi.yassine@gmail.com',	'$2y$10$/zrRU0yKnJWlAB2.C45IHehYxqzWakUqsiyXYZTDNXrJsnyP0hLrS',	'2017-06-14 10:35:14',	'2017-06-14 10:35:14',	1,	1,	NULL,	'004cba5d-488b-4d33-8443-e827bd78d5a3');

DROP TABLE IF EXISTS `users_groups`;
CREATE TABLE `users_groups` (
  `user_id` char(36) NOT NULL,
  `group_id` char(36) NOT NULL,
  PRIMARY KEY (`user_id`,`group_id`),
  KEY `user_id` (`user_id`),
  KEY `group_id` (`group_id`),
  CONSTRAINT `users_groups_ibfk_3` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `users_groups_ibfk_4` FOREIGN KEY (`group_id`) REFERENCES `groups` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `users_groups` (`user_id`, `group_id`) VALUES
('5ae2b81d-15d7-4697-b9c2-776077873a74',	'c7220616-add3-4c8f-acd3-1a0124623c0'),
('9cb4491b-6140-4d48-924f-52fed39902ef',	'5cd67264-d514-4416-ba2a-059bbcaf35c'),
('a1d82d73-a501-4dd2-8273-45a941bfa19b',	'067105d-de39-4d13-8949-b0840a75826'),
('a1d82d73-a501-4dd2-8273-45a941bfa19b',	'5cd67264-d514-4416-ba2a-059bbcaf35c'),
('a1d82d73-a501-4dd2-8273-45a941bfa19b',	'6ab563eb-5178-440e-8fcb-1f0d0185442'),
('a1d82d73-a501-4dd2-8273-45a941bfa19b',	'79e08051-2133-4a59-92c7-6cf10253e58'),
('a1d82d73-a501-4dd2-8273-45a941bfa19b',	'9b13f171-80c5-44ad-971d-6d0037e2250'),
('a1d82d73-a501-4dd2-8273-45a941bfa19b',	'c7220616-add3-4c8f-acd3-1a0124623c0'),
('a87af55b-a7ba-4efa-989a-ab0cced8382f',	'5cd67264-d514-4416-ba2a-059bbcaf35c'),
('a87af55b-a7ba-4efa-989a-ab0cced8382f',	'6de34d8e-cae3-11e7-b26c-3417ebe21fe'),
('a87af55b-a7ba-4efa-989a-ab0cced8382f',	'8daf6fc7-fe52-4dc0-8546-81b3bc4d8989'),
('a87af55b-a7ba-4efa-989a-ab0cced8382f',	'bd67be72-cae3-11e7-b26c-341ebe721d3'),
('a87af55b-a7ba-4efa-989a-ab0cced8382f',	'c7220616-add3-4c8f-acd3-1a0124623c0');

-- 2017-11-19 22:58:10
