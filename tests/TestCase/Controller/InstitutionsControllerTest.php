<?php
namespace App\Test\TestCase\Controller;

use App\Controller\InstitutionsController;
use Cake\TestSuite\IntegrationTestCase;

/**
 * App\Controller\InstitutionsController Test Case
 */
class InstitutionsControllerTest extends IntegrationTestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.institutions',
        'app.cities',
        'app.filieres',
        'app.groups',
        'app.users',
        'app.tps',
        'app.modules',
        'app.modules_groups',
        'app.tps_groups',
        'app.tps_users',
        'app.filieres_institutions'
    ];

    /**
     * Test index method
     *
     * @return void
     */
    public function testIndex()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test view method
     *
     * @return void
     */
    public function testView()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test add method
     *
     * @return void
     */
    public function testAdd()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test edit method
     *
     * @return void
     */
    public function testEdit()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test delete method
     *
     * @return void
     */
    public function testDelete()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
