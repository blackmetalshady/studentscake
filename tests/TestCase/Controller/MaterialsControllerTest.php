<?php
namespace App\Test\TestCase\Controller;

use App\Controller\MaterialsController;
use Cake\TestSuite\IntegrationTestCase;

/**
 * App\Controller\MaterialsController Test Case
 */
class MaterialsControllerTest extends IntegrationTestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.materials',
        'app.modules',
        'app.groups',
        'app.filieres',
        'app.institutions',
        'app.cities',
        'app.filieres_institutions',
        'app.users',
        'app.roles',
        'app.students',
        'app.teachers',
        'app.teachers_modules',
        'app.tps',
        'app.tps_groups',
        'app.tps_users',
        'app.users_groups',
        'app.modules_groups'
    ];

    /**
     * Test index method
     *
     * @return void
     */
    public function testIndex()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test view method
     *
     * @return void
     */
    public function testView()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test add method
     *
     * @return void
     */
    public function testAdd()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test edit method
     *
     * @return void
     */
    public function testEdit()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test delete method
     *
     * @return void
     */
    public function testDelete()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
