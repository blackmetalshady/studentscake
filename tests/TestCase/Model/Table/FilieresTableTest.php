<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\FilieresTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\FilieresTable Test Case
 */
class FilieresTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\FilieresTable
     */
    public $Filieres;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.filieres',
        'app.groups',
        'app.users',
        'app.tps',
        'app.modules',
        'app.modules_groups',
        'app.tps_groups',
        'app.tps_users'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Filieres') ? [] : ['className' => 'App\Model\Table\FilieresTable'];
        $this->Filieres = TableRegistry::get('Filieres', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Filieres);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
