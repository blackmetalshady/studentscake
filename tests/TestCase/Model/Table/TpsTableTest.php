<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\TpsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\TpsTable Test Case
 */
class TpsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\TpsTable
     */
    public $Tps;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.tps',
        'app.groups',
        'app.filieres',
        'app.users',
        'app.tps_groups'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Tps') ? [] : ['className' => 'App\Model\Table\TpsTable'];
        $this->Tps = TableRegistry::get('Tps', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Tps);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
