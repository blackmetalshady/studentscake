<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\FilieresInstitutionsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\FilieresInstitutionsTable Test Case
 */
class FilieresInstitutionsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\FilieresInstitutionsTable
     */
    public $FilieresInstitutions;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.filieres_institutions',
        'app.filieres',
        'app.groups',
        'app.users',
        'app.tps',
        'app.modules',
        'app.modules_groups',
        'app.tps_groups',
        'app.tps_users',
        'app.institutions',
        'app.cities'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('FilieresInstitutions') ? [] : ['className' => 'App\Model\Table\FilieresInstitutionsTable'];
        $this->FilieresInstitutions = TableRegistry::get('FilieresInstitutions', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->FilieresInstitutions);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
