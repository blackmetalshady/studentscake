<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\TpsGroupsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\TpsGroupsTable Test Case
 */
class TpsGroupsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\TpsGroupsTable
     */
    public $TpsGroups;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.tps_groups',
        'app.groups',
        'app.filieres',
        'app.users',
        'app.tps'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('TpsGroups') ? [] : ['className' => 'App\Model\Table\TpsGroupsTable'];
        $this->TpsGroups = TableRegistry::get('TpsGroups', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->TpsGroups);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
