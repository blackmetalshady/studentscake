<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\FilieresModulesTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\FilieresModulesTable Test Case
 */
class FilieresModulesTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\FilieresModulesTable
     */
    public $FilieresModules;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.filieres_modules',
        'app.modules',
        'app.groups',
        'app.filieres',
        'app.users',
        'app.tps',
        'app.tps_groups',
        'app.tps_users',
        'app.modules_groups'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('FilieresModules') ? [] : ['className' => 'App\Model\Table\FilieresModulesTable'];
        $this->FilieresModules = TableRegistry::get('FilieresModules', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->FilieresModules);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
