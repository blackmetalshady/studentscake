<?php
namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * MessagesFixture
 *
 */
class MessagesFixture extends TestFixture
{

    /**
     * Fields
     *
     * @var array
     */
    // @codingStandardsIgnoreStart
    public $fields = [
        'id' => ['type' => 'uuid', 'length' => null, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null],
        'from_user' => ['type' => 'uuid', 'length' => null, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null],
        'to_user' => ['type' => 'uuid', 'length' => null, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null],
        'module_id' => ['type' => 'uuid', 'length' => null, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null],
        'sent_at' => ['type' => 'datetime', 'length' => null, 'null' => false, 'default' => 'CURRENT_TIMESTAMP', 'comment' => '', 'precision' => null],
        'read' => ['type' => 'integer', 'length' => 4, 'unsigned' => false, 'null' => false, 'default' => '0', 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        '_indexes' => [
            'module_id' => ['type' => 'index', 'columns' => ['module_id'], 'length' => []],
            'to_user' => ['type' => 'index', 'columns' => ['to_user'], 'length' => []],
            'from_user' => ['type' => 'index', 'columns' => ['from_user'], 'length' => []],
        ],
        '_constraints' => [
            'primary' => ['type' => 'primary', 'columns' => ['id'], 'length' => []],
            'messages_ibfk_6' => ['type' => 'foreign', 'columns' => ['module_id'], 'references' => ['modules', 'id'], 'update' => 'cascade', 'delete' => 'restrict', 'length' => []],
            'messages_ibfk_7' => ['type' => 'foreign', 'columns' => ['to_user'], 'references' => ['users', 'id'], 'update' => 'cascade', 'delete' => 'restrict', 'length' => []],
            'messages_ibfk_8' => ['type' => 'foreign', 'columns' => ['from_user'], 'references' => ['users', 'id'], 'update' => 'cascade', 'delete' => 'restrict', 'length' => []],
        ],
        '_options' => [
            'engine' => 'InnoDB',
            'collation' => 'utf8_general_ci'
        ],
    ];
    // @codingStandardsIgnoreEnd

    /**
     * Records
     *
     * @var array
     */
    public $records = [
        [
            'id' => '2a73759a-c2a2-4897-a308-67308ab5c140',
            'from_user' => 'a11b7959-211b-4243-840f-3b478083d6ac',
            'to_user' => '69a30da0-382d-44f0-89a7-6be34f727036',
            'module_id' => 'c0cda642-b92a-4495-b6fa-7813b395c44a',
            'sent_at' => '2017-06-16 00:36:22',
            'read' => 1
        ],
    ];
}
