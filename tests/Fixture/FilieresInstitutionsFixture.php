<?php
namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * FilieresInstitutionsFixture
 *
 */
class FilieresInstitutionsFixture extends TestFixture
{

    /**
     * Fields
     *
     * @var array
     */
    // @codingStandardsIgnoreStart
    public $fields = [
        'filiere_id' => ['type' => 'uuid', 'length' => null, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null],
        'institution_id' => ['type' => 'uuid', 'length' => null, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null],
        '_indexes' => [
            'filiere_id' => ['type' => 'index', 'columns' => ['filiere_id'], 'length' => []],
            'institution_id' => ['type' => 'index', 'columns' => ['institution_id'], 'length' => []],
        ],
        '_constraints' => [
            'filieres_institutions_ibfk_1' => ['type' => 'foreign', 'columns' => ['filiere_id'], 'references' => ['filieres', 'id'], 'update' => 'restrict', 'delete' => 'restrict', 'length' => []],
            'filieres_institutions_ibfk_2' => ['type' => 'foreign', 'columns' => ['institution_id'], 'references' => ['institutions', 'id'], 'update' => 'restrict', 'delete' => 'restrict', 'length' => []],
        ],
        '_options' => [
            'engine' => 'InnoDB',
            'collation' => 'utf8_general_ci'
        ],
    ];
    // @codingStandardsIgnoreEnd

    /**
     * Records
     *
     * @var array
     */
    public $records = [
        [
            'filiere_id' => '34c8d84d-9821-4b1e-8296-8ba688276e0b',
            'institution_id' => '63b60c0a-c4cb-412a-a88e-dc244f3e19e1'
        ],
    ];
}
