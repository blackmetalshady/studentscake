<?php
namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * FilieresModulesFixture
 *
 */
class FilieresModulesFixture extends TestFixture
{

    /**
     * Fields
     *
     * @var array
     */
    // @codingStandardsIgnoreStart
    public $fields = [
        'modules_id' => ['type' => 'uuid', 'length' => null, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null],
        'filiere_id' => ['type' => 'uuid', 'length' => null, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null],
        '_indexes' => [
            'modules_id' => ['type' => 'index', 'columns' => ['modules_id'], 'length' => []],
            'filiere_id' => ['type' => 'index', 'columns' => ['filiere_id'], 'length' => []],
        ],
        '_constraints' => [
            'filieres_modules_ibfk_3' => ['type' => 'foreign', 'columns' => ['modules_id'], 'references' => ['modules', 'id'], 'update' => 'cascade', 'delete' => 'cascade', 'length' => []],
            'filieres_modules_ibfk_4' => ['type' => 'foreign', 'columns' => ['filiere_id'], 'references' => ['filieres', 'id'], 'update' => 'cascade', 'delete' => 'cascade', 'length' => []],
        ],
        '_options' => [
            'engine' => 'InnoDB',
            'collation' => 'latin1_swedish_ci'
        ],
    ];
    // @codingStandardsIgnoreEnd

    /**
     * Records
     *
     * @var array
     */
    public $records = [
        [
            'modules_id' => '31c0f384-bb2b-471f-88ea-010a7ba87cbd',
            'filiere_id' => 'af46a76c-e24e-46ac-9339-6ca958e3f661'
        ],
    ];
}
